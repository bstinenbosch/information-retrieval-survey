<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            'question' => 'Which  Dalai Lama played a big role in the discrepancy between Tibet and China and what was his role?',
            'topic' => 'Politics'
        ]);
        DB::table('questions')->insert([
            'question' => 'Who scored the first try in the 2011 Rugby World Cup and for which country?',
            'topic' => 'Sports'
        ]);
        DB::table('questions')->insert([
            'question' => 'What is my online clothing size?',
            'topic' => 'Online shopping'
        ]);
        DB::table('questions')->insert([
            'question' => 'The movie “Blue Chips” featured three real life ex-college basketball stars. What are the character names and what are the real names of those three players?',
            'topic' => 'Movies'
        ]);
        DB::table('questions')->insert([
            'question' => 'Which planet lies next to Pluto?',
            'topic' => 'Science'
        ]);
        DB::table('questions')->insert([
            'question' => 'When are plane tickets the cheapest?',
            'topic' => 'Lifestyle'
        ]);
    }
}
