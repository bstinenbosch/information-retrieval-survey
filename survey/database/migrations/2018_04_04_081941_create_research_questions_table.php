<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResearchQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('respondent_id')->nullable();
            $table->integer('question_id')->nullable();
            $table->integer('familiarity')->nullable();
            $table->integer('interest')->nullable();
            $table->integer('difficulty')->nullable();
            $table->integer('marked')->nullable();
            $table->integer('queries')->nullable();
            $table->integer('assessments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_questions');
    }
}