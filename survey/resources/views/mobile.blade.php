@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Welcome to our survey!</h4></div>

                <div class="panel-body">
                    <p>With this experiment we try to understand the relation between a persons
                        typing speed and their search behavior. Therefore this survey needs to be done on a laptop or desktop.
                        Please start this survey on your computer to start the experiment!</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection