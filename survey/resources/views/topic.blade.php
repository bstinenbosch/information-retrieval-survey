@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Topic questions</div>

                <div class="panel-body">
                    <form method="POST" action="{{ url('/savetopic') }}">
                        {{ csrf_field() }}
                        <h4>The topic is: <b>{{ $topic }}</b></h4>

                        <div class="form-group row" style="margin-left: 0px;">
                            <h4>How familiar are you with this topic?</h4>
                            <div class="col-md-2 offset-md-1 text-center">
                                <p>Not at all familiar</p>
                                <input type="radio" name="familiarity" value="1" required>
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Slightly familiar</p>
                                <input type="radio" name="familiarity" value="2">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Somewhat familiar</p>
                                <input type="radio" name="familiarity" value="3">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Moderately familiar</p>
                                <input type="radio" name="familiarity" value="4">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Extremely familiar</p>
                                <input type="radio" name="familiarity" value="5">
                            </div>
                        </div>

                        <div class="form-group row" style="margin-left: 0px;">
                            <h4>How interested are you in this topic?</h4>
                            <div class="col-md-2 offset-md-1 text-center">
                                <p>Not interested at all</p>
                                <input type="radio" name="interest" value="1" required>
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Slightly interested</p>
                                <input type="radio" name="interest" value="2">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Somewhat interested</p>
                                <input type="radio" name="interest" value="3">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Moderately interested</p>
                                <input type="radio" name="interest" value="4">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Extremely interested</p>
                                <input type="radio" name="interest" value="5">
                            </div>
                        </div>

                        <div class="form-group row" style="margin-left: 0px;">
                            <h4>How difficult do you find this topic?</h4>
                            <div class="col-md-2 offset-md-1 text-center">
                                <p>Not difficult at all</p>
                                <input type="radio" name="difficulty" value="1" required>
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Slightly difficult</p>
                                <input type="radio" name="difficulty" value="2">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Somewhat difficult</p>
                                <input type="radio" name="difficulty" value="3">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Moderately difficult</p>
                                <input type="radio" name="difficulty" value="4">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Extremely difficult</p>
                                <input type="radio" name="difficulty" value="5">
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Start the search!
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
