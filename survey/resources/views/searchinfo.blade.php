@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Search explanation</div>

                <div class="panel-body">
                    <p>In a moment, you will be presented with six search tasks
                        after each other. First, you will be presented with the
                        topics and asked a few questions about your interest
                        and familiarity with the topic. Next, the search task
                        will be provided. You will have a normal search box,
                        and can use as much queries as you like. When you
                        have executed the search you will be provided with
                        the search results. You need to mark as many relevant
                        documents (sites) as you think you will need to be
                        able to answer the question. You are allowed to open
                        the web pages if needed. The pages will be opened in a new tab. When you think you marked
                        a sufficient number of documents, you can press the
                        save button to go to the next question.</p>

                    <a href="/searchTopic"><button id="next" class="btn btn-primary">
                            Start the search!
                    </button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection