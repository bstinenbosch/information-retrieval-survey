@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Thank you for participating!</div>

                <div class="panel-body">
                    <p>This was already the last question from this experiment. Thank you for participating! If you want
                        to help us even more, please share the experiment with a friend, relative or colleague.<br>
                        <a href="http://sgboek.nl">http://sgboek.nl</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection