@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Basic information</div>

                <div class="panel-body">
                    <form method="POST" action="{{ url('/saverespondent') }}">
                        {{ csrf_field() }}
                        <h4>Before we start the searching we need some basic information.</h4>
                        <div class="form-group">
                            <label>What is your age?</label>
                            <input type="text" name="age" required>
                        </div>

                        <div class="form-group row" style="margin-left: 0px;">
                            <h4>How good are you in reading English?</h4>
                            <div class="col-md-2 offset-md-1 text-center">
                                <p>Poor</p>
                                <input type="radio" name="reading" value="1" required>
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Fair</p>
                                <input type="radio" name="reading" value="2">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Good</p>
                                <input type="radio" name="reading" value="3">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Very good</p>
                                <input type="radio" name="reading" value="4">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Excellent</p>
                                <input type="radio" name="reading" value="5">
                            </div>
                        </div>

                        <div class="form-group row" style="margin-left: 0px;">
                            <h4>How good are you in writing English?</h4>
                            <div class="col-md-2 offset-md-1 text-center">
                                <p>Poor</p>
                                <input type="radio" name="writing" value="1" required>
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Fair</p>
                                <input type="radio" name="writing" value="2">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Good</p>
                                <input type="radio" name="writing" value="3">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Very good</p>
                                <input type="radio" name="writing" value="4">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Excellent</p>
                                <input type="radio" name="writing" value="5">
                            </div>
                        </div>

                        <div class="form-group row" style="margin-left: 0px;">
                            <h4>How familiar are you with this keyboard?</h4>
                            <div class="col-md-2 offset-md-1 text-center">
                                <p>Not at all familiar</p>
                                <input type="radio" name="keyboard" value="1" required>
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Slightly familiar</p>
                                <input type="radio" name="keyboard" value="2">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Somewhat familiar</p>
                                <input type="radio" name="keyboard" value="3">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Moderately familiar</p>
                                <input type="radio" name="keyboard" value="4">
                            </div>
                            <div class="col-md-2 text-center">
                                <p>Extremely familiar</p>
                                <input type="radio" name="keyboard" value="5">
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Start the survey!
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
