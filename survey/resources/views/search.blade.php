@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6" style="padding-top: 5px;"><p>Search assignment</p></div>
                        <div class="col-md-6"><button id="next" class="btn btn-primary pull-right">next question</button></div>
                    </div>

                </div>

                <div class="panel-body">

                        <!-- Item -->
                    <div class="form-group">
                        <div class="col-md-12">
                            <h3>your question is:</h3>
                            <p>{{ $question }}</p>
                            <hr>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-md-9">
                            <input  type="text" id="query" class="form-control">
                            </input>

                        </div>
                        <div class="col-md-3">
                            <button id="search" class="btn btn-primary">
                                Search!
                            </button>
                        </div>

                    </div>

                    <table id="results">


                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        var querying = false;
        var assassing = false;
        var chars;
        var query_start_time;
        var typing_start_time;
        var query_end_time;
        var assessment_start_time;
        var assessment_end_time;
        var relavent_documents = 0;

        $(document).on('click', '#next', function () {
            if(assassing) {
                assessment_end_time = performance.now();
                var assesment_time = (assessment_end_time - assessment_start_time)/1000;
                assassing = false;

                $.ajax({
                    type: "POST",
                    url: "{{ url('/next') }}",
                    data: {
                        "_token"                : "{{ csrf_token() }}",
                        "research_question_id"  : "{{ $research_question_id }}",
                        "time"                  : assesment_time,
                        "marked"                : relavent_documents
                    }
                }).done(function(){
                    window.location.href = "{{ url('/searchTopic') }}";
                })
            } else {
                alert("Search before you go to the next question!");
            }

            return false;
        });

        $("#query").click(function () {
            if(!querying) {
                query_start_time = performance.now();
            }
            if(assassing) {
                assessment_end_time = performance.now();
                var assesment_time = (assessment_end_time - assessment_start_time)/1000;
                assassing = false;

                $.ajax({
                    type: "POST",
                    url: "{{ url('/assessment') }}",
                    data: {
                        "_token"                : "{{ csrf_token() }}",
                        "research_question_id"  : "{{ $research_question_id }}",
                        "time"                  : assesment_time
                    }
                })
            }
        });
        
        $("#query").keyup(function(event) {
            if(!querying) {
                querying = true;
                typing_start_time = performance.now();
                chars= 0;
            }
            if (event.keyCode === 13) {
                search()
                return;
            }
            chars++;
        });

        $(document).on('click', '#search', function () {
            search()
            return false;
        });

        function search() {
            if ($('#query').val().length > 0) {
                query_end_time = performance.now();
                querying=false;
                $('#results').empty();
                var search_query = $('#query').val().split(' ').join('+');
                var query_length = $('#query').val().length;
                var error_chars = chars - query_length;
                var query_time = (query_end_time - query_start_time)/1000;
                var type_time = (query_end_time - typing_start_time)/1000;
                var wpm = ((chars/5)/(type_time/60));
                $('#query').val("");

                $.ajax({
                    type: "POST",
                    url: "{{ url('/query') }}",
                    data: {
                        "_token"                : "{{ csrf_token() }}",
                        "search_query"                 : search_query,
                        "research_question_id"  : "{{ $research_question_id }}",
                        "time"                  : query_time,
                        "characters"            : query_length,
                        "errors"                : error_chars,
                        "wpm"                   : wpm
                    }
                })
                    .done(function(results){
                        //clean the table
                        results = JSON.parse(results);
                        //fill the results
                        for(var i = 0; i < results.length; i++) {
                            var result = results[i];
                            var html = '<hr><div class="row" style="padding-top: 5px;"><div class="col-md-2 text-center">\n' +
                                '\n' +
                                '<label>Relevant</label><br>' +
                                '<input type="checkbox" value="" class="relavent_check"></div>\n' +
                                '<div class="col-md-10" style="margin-top: -10px;"><a href="' + result.link + '" target="_app">' +
                                '' + result.title + '</a>' +
                                '<p style="color: #006621; font-size: 14px; margin: 0; padding:0">' + result.url + '</p>' +
                                '' + result.desciption + '</div></div>';

                            $('#results').append(html);

                        }
                        $('.relavent_check').click(function() {
                            if ($(this).is(':checked')) {
                                relavent_documents++;
                            } else {
                                relavent_documents--;
                            }
                            console.log("documents checked: " + relavent_documents);
                        });

                        //start the assessment time
                        assessment_start_time = performance.now();
                        assassing = true;
                    });
            }
        }


    });
</script>
@endsection