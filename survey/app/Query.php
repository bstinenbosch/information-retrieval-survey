<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
    protected $fillable = ['research_question_id', 'time', 'characters', 'errors', 'wpm'];
}
