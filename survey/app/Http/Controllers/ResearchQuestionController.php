<?php

namespace App\Http\Controllers;

use App\ResearchQuestion;
use Illuminate\Http\Request;

class ResearchQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ResearchQuestion  $researchQuestion
     * @return \Illuminate\Http\Response
     */
    public function show(ResearchQuestion $researchQuestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ResearchQuestion  $researchQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit(ResearchQuestion $researchQuestion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ResearchQuestion  $researchQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResearchQuestion $researchQuestion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ResearchQuestion  $researchQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResearchQuestion $researchQuestion)
    {
        //
    }
}
