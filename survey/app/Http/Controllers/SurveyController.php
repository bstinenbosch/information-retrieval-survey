<?php

namespace App\Http\Controllers;

use App\Assessment;
use App\Query;
use App\Question;
use App\ResearchQuestion;
use App\Respondent;
use App\Result;
use DOMDocument;
use DOMXPath;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    public function welcome()
    {
        //Make new respondent and set session respondent id
        $respondent = Respondent::create();
        session(['respondent_id' => $respondent->id]);

        return view('welcome');
    }

    public function respondent()
    {
        //Get the correct respondent
        $respondent = Respondent::find(session('respondent_id'));

        return view('respondent');
    }

    public function saveRespondent(Request $request)
    {
        $respondent = Respondent::find(session('respondent_id'));
        $respondent->age = $request->age;
        $respondent->reading = $request->reading;
        $respondent->writing = $request->writing;
        $respondent->keyboard = $request->keyboard;
        $respondent->save();

        return redirect('searchinfo');

    }

    public function searchInfo()
    {
        return view('searchinfo');
    }

    public function topic()
    {
        $respondent = Respondent::find(session('respondent_id'));

        $questions = $respondent->questions();

        if (empty($questions)) {
            return view('end');
        }

        $questionId = $questions[array_rand($questions, 1)];

        session(['question_id' => $questionId]);
        $question = Question::find($questionId);

        return view('topic', ['topic' => $question->topic]);
    }

    //todo: Implement and add to route
    public function saveTopic(Request $request)
    {
        //make new research question
        $researchQuestion = ResearchQuestion::create();

        //add the data
        $researchQuestion->respondent_id    = session('respondent_id');
        $researchQuestion->question_id      = session('question_id');
        $researchQuestion->familiarity      = $request->familiarity;
        $researchQuestion->interest         = $request->interest;
        $researchQuestion->difficulty       = $request->difficulty;

        //save the data
        $researchQuestion->save();

        //add the current research question to the session
        session(['research_question_id' => $researchQuestion->id]);

        //redirect to the search screen
        return redirect('search');

    }

    public function search()
    {
        //find the correct question
        $question = Question::find(session('question_id'));

        //return the screen with the correct question
        return view('search', [
            'question' => $question->question,
            'research_question_id' => session('research_question_id')
        ]);
    }

    public function assess(Request $request)
    {
        //make new query
        $assessment = Assessment::create(request(['research_question_id', 'time']));

        //save the data
        $assessment->save();

        return "OK";
    }

    public function next(Request $request)
    {
        //make new query
        $assessment = Assessment::create(request(['research_question_id', 'time']));
        $assessment->save();

        //Complete the search question
        $researchQuestion = ResearchQuestion::find($request->research_question_id);
        $researchQuestion->marked       = $request->marked;
        $researchQuestion->queries      = \DB::table('queries')->where('research_question_id', '=', $request->research_question_id)->count();
        $researchQuestion->assessments  = \DB::table('assessments')->where('research_question_id', '=', $request->research_question_id)->count();
        $researchQuestion->save();

        return "OK";
    }

    public function query(Request $request)
    {
        /**
         * Saving the query
         */
        //make new query
        $query = Query::create(request(['research_question_id', 'time', 'characters', 'errors', 'wpm']));

        //save the data
        $query->save();

        /**
         * Return the result
         */
        //Get the page
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, 'https://duckduckgo.com/html/?q=' . $request->search_query);
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $html = curl_exec($ch);

        //Store it in a DOMDocument
        $doc = new DOMDocument();
        libxml_use_internal_errors(TRUE);
        @$doc->loadHTML($html);
        libxml_clear_errors();

        //find results
        $finder = new DomXPath($doc);
        $classname="result__body";
        $items = $finder->query("//*[contains(@class, '$classname')]");;

        $results = array();

        //display all H1 text
        for ($i = 0; $i < $items->length; $i++) {
            $result = new Result();
            $node = $items->item($i);
            $result->title = $node->getElementsByTagName('h2')->item(0)->getElementsByTagName('a')->item(0)->nodeValue;
            $res = $node->getElementsByTagName('h2')->item(0);
            $result->link = $node->getElementsByTagName('h2')->item(0)->getElementsByTagName('a')->item(0)->getAttribute("href");

            //Get the description
            $nodes = $node->getElementsByTagName("a");
            foreach ($nodes as $element)
            {
                $class = $element->getAttribute("class");
                if (strpos($class, "result__snippet") !== false)
                {
                    $result->desciption = $element->nodeValue;
                }

            }

            //Get the url
            $nodes = $node->getElementsByTagName("a");
            foreach ($nodes as $element)
            {
                $class = $element->getAttribute("class");
                if (strpos($class, "result__url") !== false)
                {
                    $result->url = $element->nodeValue;
                }

            }
            $results[] = $result;
        }

        return(json_encode($results));
    }
}
