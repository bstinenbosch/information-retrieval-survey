<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResearchQuestion extends Model
{
    protected $fillable = ['respondent_id', 'question_id', 'familiarity', 'interest', 'difficulty', 'marked', 'queries', 'assessments'];

    public function queries()
    {
        return $this->hasMany('Query');
    }

    public function assessments()
    {
        return $this->hasMany('Assessment');
    }
}
