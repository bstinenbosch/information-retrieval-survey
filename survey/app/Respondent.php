<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respondent extends Model
{
    protected $fillable = ['age', 'reading', 'writing', 'keyboard'];

    public function questions()
    {
        $ids = array(1,2,3,4,5,6);
        $questions =  $this->hasMany('App\ResearchQuestion')->get();

        //remove the id if the quesion is already asked
        foreach ($questions as $question) {
            if (($key = array_search($question->question_id, $ids)) !== false) {
                unset($ids[$key]);
            }
        }

        return $ids;
    }
}
