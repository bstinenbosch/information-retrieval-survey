<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\ResearchQuestion;
use App\Result;

Route::get('/', 'SurveyController@welcome');
Route::get('/respondent', 'SurveyController@respondent');
Route::Post('/saverespondent', 'SurveyController@saveRespondent');
Route::get('/searchinfo', 'SurveyController@searchInfo');
Route::get('/searchTopic', 'SurveyController@topic');
Route::Post('/savetopic', 'SurveyController@saveTopic');
Route::get('/search', 'SurveyController@search');

Route::post('/query', 'SurveyController@query');
Route::post('/assessment', 'SurveyController@assess');
Route::post('/next', 'SurveyController@next');

Route::get('/mobile', function () {
    return view('mobile');
});